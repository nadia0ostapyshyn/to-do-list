const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  id: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  done: {
    type: Boolean,
    required: true,
  },
  category: {
    type: String,
    required: false,
  },
  interval: {
    type: String,
    required: false,
  },
  timeSpent: {
    type: Number,
    required: false,
  },
});

const dbDateSchema = new Schema({
  date: {
    type: String,
    required: true,
  },
  tasks: [TaskSchema],
});

const dbDate = mongoose.model("dates", dbDateSchema);
module.exports = dbDate;
