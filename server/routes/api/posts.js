const express = require('express');
const dbDate = require('../models/Date');
const mongoose = require('mongoose');
const router = express.Router();

const dbConnect = async () => {
  const dbURI =
    'mongodb+srv://kursova:nadiiakursova@cluster0.gsd42.mongodb.net/todolist?retryWrites=true&w=majority';

  await mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};

// GET ALL DATES
router.get('/', async (req, res) => {
  await dbConnect();
  res.send(await dbDate.find());
  await mongoose.disconnect();
});

// REPLACE ALL DATES WITH REQUEST
router.post('/', async (req, res) => {
  await dbConnect();

  await dbDate.deleteMany({});

  await dbDate.insertMany(req.body);

  res.status(201).send();
  await mongoose.disconnect();
});

module.exports = router;
