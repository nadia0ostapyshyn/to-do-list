import { Select } from './Select.js';

export let selectedCategory = '';

export const selectCategory = new Select('#selectCategory', {
  placeholder: 'Category',
  data: [
    { id: '1', value: 'Study' },
    { id: '2', value: 'Housework' },
    { id: '3', value: 'University hw' },
    { id: '4', value: 'Tutorials' },
    { id: '5', value: 'Cooking' },
  ],
  onSelect(item) {
    selectedCategory = item.value;
  },
  isNeedOtherCategory: true,
});

export let selectedInterval = '';

export const selectInterval = new Select('#selectInterval', {
  placeholder: 'Interval',
  data: [
    { id: '1', value: 'Every day' },
    { id: '2', value: 'Every week' },
    { id: '3', value: 'Every month' },
  ],
  onSelect(item) {
    selectedInterval = item.value;
  },
  isNeedOtherCategory: false,
});

export function resetAllSelects() {
  const selectCategoryInput = document.querySelector('#selectCategory span');
  const selectIntervalInput = document.querySelector('#selectInterval span');

  selectedCategory = '';
  selectCategoryInput.textContent = selectCategory.options.placeholder;

  selectedInterval = '';
  selectIntervalInput.textContent = selectInterval.options.placeholder;
}
