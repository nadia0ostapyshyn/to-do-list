const head = document.getElementsByTagName('HEAD')[0];
const link = document.createElement('link');
link.rel = 'stylesheet';
link.href = './js/components/select/styles.css';
head.appendChild(link);

const getTemplate = (
  data = [],
  placeholder,
  selectedId,
  isNeedOtherCategory
) => {
  let text = placeholder ?? 'Placeholder by default';

  const items = data.map((item) => {
    let cls = '';

    if (item.id === selectedId) {
      text = item.value;
      cls = 'selected';
    }

    return `
      <li class="select__item ${cls}" data-type="item" data-id="${item.id}">${item.value}</li>
    `;
  });

  return `
    <div class="select__backdrop" data-type="backdrop"></div>
    <div class="select__input" data-type="input">
    <span data-type="value">${text}</span>
    <i class="fas fa-chevron-down" data-type="arrow"></i>
    </div>
    <div class="select__dropdown">
      <ul class="select__list">
        ${items.join('')}

        ${
          isNeedOtherCategory
            ? '<li class="select__item " data-type="add-new">Other Category</li>'
            : ''
        }
      </ul>
    </div>
  `;
};

export class Select {
  constructor(selector, options) {
    this.$el = document.querySelector(selector);
    this.options = options;
    this.selectedId = options.selectedId;

    this.#render();
    this.#setup();
  }

  #render() {
    const { placeholder, data } = this.options;
    this.$el.classList.add('select');
    this.$el.innerHTML = getTemplate(
      data,
      placeholder,
      this.selectedId,
      this.options.isNeedOtherCategory
    );
  }

  #setup() {
    this.$el.removeEventListener('click', this.clickHandler);
    this.clickHandler = this.clickHandler.bind(this);
    this.$el.addEventListener('click', this.clickHandler);
    this.$arrow = this.$el.querySelector('[data-type="arrow"]');
    this.$value = this.$el.querySelector('[data-type="value"]');
  }

  clickHandler(e) {
    const { type } = e.target.dataset;

    if (type === 'input' || type === 'value' || type === 'arrow') {
      this.toggle();
    } else if (type === 'item') {
      const id = e.target.dataset.id;
      this.select(id);
    } else if (type === 'backdrop') {
      this.close();
    } else if (type === 'add-new') {
      this.addNew();
    }
  }

  get isOpen() {
    return this.$el.classList.contains('open');
  }

  get current() {
    return (
      this.options.data.find((item) => item.id === this.selectedId) || null
    );
  }

  select(id) {
    this.selectedId = id;

    this.$value.textContent = this.current.value;

    this.$el.querySelectorAll(`[data-type="item"]`).forEach((el) => {
      el.classList.remove('selected');
    });
    this.$el.querySelector(`[data-id="${id}"]`).classList.add('selected');

    this.options.onSelect ? this.options.onSelect(this.current) : null;

    this.close();
  }

  addNew() {
    const newCategory = prompt('Add New Category', '');
    this.options.data.push({
      id: (this.options.data.length + 1).toString(),
      value: newCategory,
    });

    this.#render();
    this.#setup();
    this.select(this.options.data[this.options.data.length - 1].id);
  }

  toggle() {
    this.isOpen ? this.close() : this.open();
  }

  open() {
    this.$el.classList.add('open');
    this.$arrow.classList.remove('fa-chevron-down');
    this.$arrow.classList.add('fa-chevron-up');
  }

  close() {
    this.$el.classList.remove('open');
    this.$arrow.classList.add('fa-chevron-down');
    this.$arrow.classList.remove('fa-chevron-up');
  }
}
