import { SingleTask } from "../SingleTask/SingleTask.js";

export const TasksList = (tasks = []) => {
  return tasks
    .map((task) => {
      return SingleTask(task);
    })
    .join("");
};
