import { timerModal, clickedDayData } from '../ModalWindow/ModalWindow.js';
import { findTaskById } from '../../helpers/findTaskById.js';
import { setTaskRequest } from '../../services/tasks.js';
import { items } from '../../store/index.js';

let countdown;
const timerDisplay = document.querySelector('.display__time-left');
const endTime = document.querySelector('.display__end-time');
const buttons = document.querySelectorAll('[data-time]');

let taskForTimer = null;
let spentTimeDiv = null;
export function timeTask(e) {
  if (!e.target.matches('.fa-clock') && !e.target.matches('.category')) return;
  const index = e.target.closest('li').querySelector('input').dataset.index;
  taskForTimer = findTaskById(clickedDayData, index);

  const taskDiv = document.querySelector('.task-info');
  taskDiv.textContent = taskForTimer.text;
  timerModal.style.setProperty('left', '10%');

  spentTimeDiv = e.target.closest('li').querySelector('.time-spent');

  showSpentTime(spentTimeDiv, taskForTimer.timeSpent);
}

const finishButton = timerModal.querySelector('.action__finish');
finishButton.addEventListener('click', finishEarlier);

let secondsLeft = 0;
let seconds = 0;
function timer(seconds) {
  clearInterval(countdown);

  const now = Date.now();
  const then = now + seconds * 1000;
  displayTimeLeft(seconds);
  displayEndTime(then);

  countdown = setInterval(() => {
    secondsLeft = Math.round((then - Date.now()) / 1000);

    if (secondsLeft < 0) {
      clearInterval(countdown);
      return;
    }

    displayTimeLeft(secondsLeft);

    const audio = new Audio();
    if (secondsLeft === -0) {
      audio.src = 'timer-end.wav';
      audio.autoplay = true;

      calcSpentTime(seconds);
    }
  }, 1000);
}

function displayTimeLeft(seconds) {
  const minutes = Math.floor(seconds / 60);
  const remainderSeconds = seconds % 60;
  const display = `${minutes}:${
    remainderSeconds < 10 ? '0' : ''
  }${remainderSeconds}`;
  document.title = display;
  timerDisplay.textContent = display;
}

function displayEndTime(timestamp) {
  const end = new Date(timestamp);
  const hours = end.getHours();
  const minutes = end.getMinutes();
  endTime.textContent = `Be back at ${hours}:${
    minutes < 10 ? '0' : ''
  }${minutes}`;
}

function startTimer() {
  seconds = parseInt(this.dataset.time);
  timer(seconds);
}

function finishEarlier() {
  calcSpentTime(seconds - secondsLeft);

  displayTimeLeft(0);
  clearInterval(countdown);
  endTime.innerHTML = '';
}

function calcSpentTime(time) {
  taskForTimer.timeSpent += time;
  setTaskRequest(items);

  showSpentTime(spentTimeDiv, taskForTimer.timeSpent);
}

function showSpentTime(spentTimeDiv, timestamp) {
  spentTimeDiv.style.setProperty('opacity', '1');

  const hours = Math.floor(timestamp / 60 / 60);
  const minutes = Math.floor(timestamp / 60) - hours * 60;
  const seconds = timestamp % 60;

  const formatted = [
    hours.toString().padStart(2, '0'),
    minutes.toString().padStart(2, '0'),
    seconds.toString().padStart(2, '0'),
  ].join(':');

  spentTimeDiv.innerHTML = formatted;
}

buttons.forEach((button) => button.addEventListener('click', startTimer));
document.customForm.addEventListener('submit', function (e) {
  e.preventDefault();
  const mins = this.minutes.value;
  seconds = mins * 60;
  timer(seconds);
  this.reset();
});

document.querySelector('.action__close').addEventListener('click', () => {
  timerModal.style.setProperty('left', '-100%');
});
