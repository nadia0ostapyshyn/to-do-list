import { RenderWeeks } from '../RenderWeeks/RenderWeeks.js';
import { months } from '../../projectVariables.js';

const date = new Date();

const RenderCalendar = () => {
  date.setDate(1);

  const monthDays = document.querySelector('.days');

  const year = date.getFullYear();
  const month = date.getMonth();

  const lastDay = new Date(year, month + 1, 0).getDate();
  const prevLastDay = new Date(year, month, 0).getDate();

  const firstDayIndex = date.getDay();
  const lastDayIndex = new Date(year, month + 1, 0).getDay();

  const nextDays = 7 - lastDayIndex - 1;

  document.querySelector('.date h1').innerHTML =
    months[date.getMonth()] + ' ' + date.getFullYear();

  const firstDayOfCurrentMonth = new Date(year, month, 1);
  const lastDayOfCurrentMonth = new Date(year, month + 1, 0);

  document
    .querySelector('.date h1')
    .setAttribute(
      'data-date',
      `${firstDayOfCurrentMonth.toLocaleDateString(
        'en-US'
      )} - ${lastDayOfCurrentMonth.toLocaleDateString('en-US')}`
    );

  let days = '';

  for (let x = firstDayIndex; x > 0; x--) {
    if (date.getMonth() !== 0) {
      days += `<li data-date="${date.getMonth()}/${
        prevLastDay - x + 1
      }/${date.getFullYear()}" class="prev-date">${prevLastDay - x + 1}</li>`;
    } else {
      days += `<li data-date="12/${prevLastDay - x + 1}/${
        date.getFullYear() - 1
      }" class="prev-date">${prevLastDay - x + 1}</li>`;
    }
  }

  for (let i = 1; i <= lastDay; i++) {
    if (
      i === new Date().getDate() &&
      date.getMonth() === new Date().getMonth()
    ) {
      days += `<li  data-date="${
        date.getMonth() + 1
      }/${i}/${date.getFullYear()}" class="today">${i}</li>`;
    } else {
      days += `<li data-date="${
        date.getMonth() + 1
      }/${i}/${date.getFullYear()}" >${i}</li>`;
    }
  }

  for (let j = 1; j <= nextDays; j++) {
    if (date.getMonth() !== 12) {
      days += `<li data-date="${
        date.getMonth() + 2
      }/${j}/${date.getFullYear()}" class="next-date">${j}</li>`;
    } else {
      days += `<li data-date="1/${j}/${
        date.getFullYear() + 1
      }" class="next-date">${j}</li>`;
    }
  }

  monthDays.innerHTML = days;
};

document.querySelector('.prev').addEventListener('click', () => {
  date.setMonth(date.getMonth() - 1);
  RenderCalendar();
  RenderWeeks();
});

document.querySelector('.next').addEventListener('click', () => {
  date.setMonth(date.getMonth() + 1);
  RenderCalendar();
  RenderWeeks();
});

export { RenderCalendar };
