export const SingleTask = (task) => {
  return `
    <li>
      <div>
        <input type="checkbox" data-index=${task.id} id="item${task.id}"
        ${task.done ? 'checked' : ''}/>
        <label for="item${task.id}">${task.text}</label>

        <div class="time-spent">${task.timeSpent}</div>
      </div>

      <div class="task__right">
        <div class="category">${task.category}</div>
        <div class="interval">${task.interval}</div>
        <div class="buttons">
          <i class="far fa-clock"></i>
          <i class="far fa-edit"></i>
          <i class="far fa-trash-alt"></i>
        </div>
      </div>
    </li>
  `;
};
