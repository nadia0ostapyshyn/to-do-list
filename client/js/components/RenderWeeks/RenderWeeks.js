Date.prototype.getWeek = function () {
  const firstJan = new Date(this.getFullYear(), 0, 1);
  return Math.ceil(((this - firstJan) / 86400000 + firstJan.getDay() + 1) / 7);
};

export const RenderWeeks = () => {
  const weekNumbersDivs = document.querySelector('.week-numbers');

  const days = document.querySelectorAll('.days li');

  let weekNumbers = '';
  for (let k = 0; k < days.length; k += 7) {
    weekNumbers += `<li data-date="${days[k].dataset.date} - ${
      days[k + 6].dataset.date
    }">${new Date(days[k].dataset.date).getWeek()}th week</li>`;
  }
  weekNumbersDivs.innerHTML = weekNumbers;
};
