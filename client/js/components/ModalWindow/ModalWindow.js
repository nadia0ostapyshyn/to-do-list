import { items } from '../../store/index.js';
import { getTasksByDate } from '../../helpers/getTasksByDate.js';
import { populateList } from '../../helpers/populateList.js';
import { setTaskRequest } from '../../services/tasks.js';
import { DAY_NOT_FOUND } from '../../projectVariables.js';

const calendar = document.querySelector('.calendar');
const modalWindow = document.querySelector('.dayModal');
const progressBar = document.querySelector('.progress-bar');
const timerModal = document.querySelector('.timerModal');
const modalWindowTitle = document.querySelector('.dayModal h1');
const tasksList = document.querySelector('.tasks');

export let clickedDate = '';
export let clickedDayData = null;

function displayItems(date) {
  clickedDayData = getTasksByDate(date);
}

function dayModalWindow(e) {
  e.stopPropagation();

  clickedDate = e.target.dataset.date;

  if (e.target.tagName === 'LI' || e.target.tagName === 'H1') {
    modalWindowTitle.innerHTML = clickedDate;
    modalWindow.classList.add('open');
  }

  if (/[-]/.test(clickedDate)) {
    const selectInterval = document.querySelector('#selectInterval');
    selectInterval.style.setProperty('display', 'none');
  } else {
    selectInterval.style.removeProperty('display');
  }

  displayItems(clickedDate);
  populateList(clickedDayData.tasks, tasksList);
  updatePercentage();

  const isDateAlreadyExist = getTasksByDate(clickedDate);
  if (isDateAlreadyExist === DAY_NOT_FOUND) {
    items.push({
      date: clickedDate,
      tasks: [],
    });
  }

  setTaskRequest(items);
}

function updatePercentage() {
  const progressBar = document.querySelector('.progress-bar');

  let totalTasks = clickedDayData?.tasks?.length || 0;
  let doneTasks = 0;

  clickedDayData?.tasks?.forEach((task) => {
    if (task.done) {
      doneTasks++;
    }
  });

  const barPercentage = (100 - 100 / (totalTasks / doneTasks)) / 100;

  progressBar.style.setProperty('top', '50px');
  if (isNaN(barPercentage)) {
    progressBar.style.setProperty('--heightWhiteBar', `650px`);
  } else {
    progressBar.style.setProperty(
      '--heightWhiteBar',
      `${650 * barPercentage}px`
    );
  }
}

calendar.addEventListener('click', dayModalWindow);

const closeModalBtn = document.querySelector('.close');

modalWindow.addEventListener('click', (e) => {
  e.stopPropagation();
});
timerModal.addEventListener('click', (e) => {
  e.stopPropagation();
});
progressBar.addEventListener('click', (e) => {
  e.stopPropagation();
});

closeModalBtn.addEventListener('click', () => {
  modalWindow.classList.remove('open');
  document.querySelector('.progress-bar').style.setProperty('top', '-100%');
  timerModal.style.setProperty('left', '-100%');
});

document.body.addEventListener('click', () => {
  modalWindow.classList.remove('open');
  document.querySelector('.progress-bar').style.setProperty('top', '-100%');
  timerModal.style.setProperty('left', '-100%');
});

function setClickedDayData(data) {
  clickedDayData = data;
}

export { dayModalWindow, updatePercentage, timerModal, setClickedDayData };
