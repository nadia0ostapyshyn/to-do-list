import { getTaskRequest } from "../services/tasks.js";

export let items = await getTaskRequest();

export function setItems(arr) {
  items = arr;
}
