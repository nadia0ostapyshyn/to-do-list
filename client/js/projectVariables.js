export const DAY_NOT_FOUND = -1;

export const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const everyDayInterval = 'Every day';
export const everyWeekInterval = 'Every week';
export const everyMonthInterval = 'Every month';
