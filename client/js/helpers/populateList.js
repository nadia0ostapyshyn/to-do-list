import { items } from "../store/index.js";
import { TasksList } from "../components/TasksList/TasksList.js";

export function populateList(tasks = [], tasksList) {
  tasksList.innerHTML = TasksList(tasks, items);
}
