export function findTaskById(day, id) {
  return day.tasks.find((task) => task.id === id);
}
