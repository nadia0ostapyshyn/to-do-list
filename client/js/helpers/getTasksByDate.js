import { items } from '../store/index.js';
import { DAY_NOT_FOUND } from '../projectVariables.js';

export function getTasksByDate(date) {
  return items.find((item) => item.date === date) || DAY_NOT_FOUND;
}
