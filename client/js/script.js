import { setTaskRequest } from '/client/js/services/tasks.js';
import {
  selectedCategory,
  selectedInterval,
  resetAllSelects,
} from '/client/js/components/Select/SelectInitialization.js';
import { generateId } from '/client/js/helpers/generateId.js';
import { RenderCalendar } from '/client/js/components/Calendar/RenderCalendar.js';
import { RenderWeeks } from '/client/js/components/RenderWeeks/RenderWeeks.js';
import {
  updatePercentage,
  clickedDayData,
  clickedDate,
  setClickedDayData,
} from '/client/js/components/ModalWindow/ModalWindow.js';
import { getTasksByDate } from '/client/js/helpers/getTasksByDate.js';
import { items, setItems } from '/client/js/store/index.js';
import { populateList } from '/client/js/helpers/populateList.js';
import {
  DAY_NOT_FOUND,
  everyDayInterval,
  everyWeekInterval,
  everyMonthInterval,
} from '/client/js/projectVariables.js';
import { findTaskById } from '/client/js/helpers/findTaskById.js';
import { timeTask } from '/client/js/components/Timer/Timer.js';

RenderCalendar();
RenderWeeks();

const form = document.querySelector('.add-task');
const tasksList = document.querySelector('.tasks');

function addItem(e) {
  e.preventDefault();

  const taskText = this.querySelector('[name="item"]').value;
  const categoryName = selectedCategory ? selectedCategory : '';
  const interval = selectedInterval ? selectedInterval : '';

  const singleTask = {
    id: generateId(),
    text: taskText,
    done: false,
    category: categoryName,
    interval: interval,
    timeSpent: 0,
  };

  setClickedDayData(getTasksByDate(clickedDate));
  clickedDayData.tasks.push(singleTask);

  setClickedDayData(getTasksByDate(clickedDate));
  populateList(clickedDayData.tasks, tasksList);

  resetAllSelects();
  this.reset();

  if (interval !== '') {
    showIntervalPrompt(clickedDate, singleTask, interval);
  }

  setTaskRequest(items);

  updatePercentage();
}

function showIntervalPrompt(date, task, interval) {
  const numberOfRepetitions = prompt(
    'How many times do you want it to repeat?'
  );
  const startDate = new Date(date);
  let nextDate = startDate;

  for (let i = 1; i < numberOfRepetitions; i++) {
    if (interval === everyDayInterval) {
      nextDate.setDate(startDate.getDate() + 1);
    }

    if (interval === everyWeekInterval) {
      nextDate.setDate(startDate.getDate() + 7);
    }

    if (interval === everyMonthInterval) {
      nextDate.setMonth(startDate.getMonth() + 1);
    }

    const stringDate = nextDate.toLocaleDateString('en-US');
    const dateWithTasks = getTasksByDate(stringDate);
    const currentTask = {
      id: generateId(),
      text: task.text,
      done: task.done,
      category: task.category,
      interval: task.interval,
    };

    if (dateWithTasks === DAY_NOT_FOUND) {
      items.push({
        date: stringDate,
        tasks: [currentTask],
      });
    } else {
      dateWithTasks.tasks.push(currentTask);
    }
  }
}

function toggleDone(e) {
  if (!e.target.matches('input')) return;
  const index = e.target.closest('li').querySelector('input').dataset.index;
  const currentTask = findTaskById(clickedDayData, index);

  currentTask.done = !currentTask.done;

  const taskDate = clickedDate;
  setTaskRequest(items);
  setClickedDayData(getTasksByDate(taskDate));
  populateList(clickedDayData.tasks, tasksList);

  updatePercentage();
}

function deleteItem(e) {
  if (!e.target.matches('.fa-trash-alt')) return;
  const index = e.target.closest('li').querySelector('input').dataset.index;

  setItems(
    items.map((day) => {
      return { ...day, tasks: day.tasks.filter((task) => task.id !== index) };
    })
  );

  setTaskRequest(items);

  const taskDate = clickedDate;
  setClickedDayData(getTasksByDate(taskDate));
  populateList(clickedDayData.tasks, tasksList);

  updatePercentage();
}

function editItem(e) {
  if (!e.target.matches('.fa-edit') && !e.target.matches('.category')) return;
  const index = e.target.closest('li').querySelector('input').dataset.index;
  const currentTask = findTaskById(clickedDayData, index);

  let editValue;
  let editDiv;

  if (e.target.matches('.fa-edit')) {
    editDiv = e.target.closest('li').querySelector('label');

    editValue = prompt('Edit', editDiv.innerHTML);
    currentTask.text = editValue;
  } else if (e.target.matches('.category')) {
    editDiv = e.target.closest('li').querySelector('.category');

    editValue = prompt('Edit', editDiv.innerHTML);
    currentTask.category = editValue;
  }

  if (editValue !== null) {
    editDiv.innerHTML = editValue;
  } else {
    return 0;
  }

  const taskDate = clickedDate;
  setTaskRequest(items);
  setClickedDayData(getTasksByDate(taskDate));
  populateList(clickedDayData.tasks, tasksList);
}

form.addEventListener('submit', addItem);
tasksList.addEventListener('click', toggleDone);
tasksList.addEventListener('click', deleteItem);
tasksList.addEventListener('click', editItem);
tasksList.addEventListener('click', timeTask);
