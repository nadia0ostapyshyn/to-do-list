export const getTaskRequest = async () => {
  const response = await fetch('http://localhost:5000/api/posts');

  return await response.json();
};

export const setTaskRequest = async (dates) => {
  return await fetch('http://localhost:5000/api/posts', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(dates),
  });
};
